pylibtiff (0.6.1-2) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.7.0, no changes.

  [ Antonio Valentino ]
  * debian/patches:
    - 0003-Compatibility-with-numpy2.patch (Closes: #1094332).
  * Update dates in d/copyright.
  * debian/rules:
    - Call dh_numpy3 -p python3-libtiff explicitly.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 28 Jan 2025 21:10:50 +0000

pylibtiff (0.6.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    - drop d/patches/0003-Force-textual-cli.patch, applied upstream.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 23 Sep 2023 17:18:15 +0000

pylibtiff (0.5.1-6) unstable; urgency=medium

  * debian/rules:
    - Skip tests requiring a large amount of memory.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Wed, 16 Aug 2023 14:58:23 +0000

pylibtiff (0.5.1-5) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Include architecture.mk for DEB_BUILD_ARCH.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Wed, 16 Aug 2023 11:13:52 +0000

pylibtiff (0.5.1-4) unstable; urgency=medium

  * debian/rules:
    - Disable autopkgtests on unsupported architectures.
      The test suite seems to be broken on BE atchitectures
      and platforms not supporting memmaps.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 14 Aug 2023 17:33:54 +0000

pylibtiff (0.5.1-3) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.6.2, no changes.
  * Bump debhelper compat to 13.
  * Remove generated files in clean target.
    (closes: #1047274)

  [ Antonio Valentino ]
  * debian/control:
    - Use the <!nocheck> marker.
    - No longer suggests tifffile.
  * Switch to autopkgtest-pkg-pybuild.
  * Update dates in d/copyright.
  * New d/python3-libtiff.lintian-overrides.
  * debian/rules:
    - Enable testing.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 14 Aug 2023 15:31:18 +0000

pylibtiff (0.5.1-2) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Use python3-setuptools-scm to create version.py.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 03 Jan 2023 09:07:13 +0000

pylibtiff (0.5.1-1) unstable; urgency=medium

  * Upload into unstable.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 02 Jan 2023 08:07:51 +0000

pylibtiff (0.5.1-1~exp1) experimental; urgency=medium

  [ Antonio Valentino ]
  * New upstream release.
  * Update d/watch to use github tags.
  * Drop d/source/options, no longer needed.
  * debian/patches:
    - drop 0004-Do-not-install-cli-tools.patch, no longer needed
    - drop 0003-Fix-README-path.patch, fixed upstream
    - refresh remaining patches
    - new 0003-Force-textual-cli.patch.
  * New pylibtiff package for command line utilities.

  [ Bas Couwenberg ]
  * Enable numpy3 dh helper.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Fri, 30 Dec 2022 11:18:20 +0000

pylibtiff (0.5.0-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.6.1, no changes.
  * Update watch file to not match post releases.

  [ Antonio Valentino ]
  * New upstream release.
  * debian/copyright:
    - fix d/copyright formatting
    - drop no longer needed Files-Exclude field
    - update copytight date.
  * debian/control:
    - add build-dependency on pybuild-plugin-pyproject.
  * debian/patches:
    - refresh and renumber all patches
    - new 0003-Fix-README-path.patch
    - new 0004-Do-not-install-cli-tools.patch.
  * debian/rules:
    - drop unnecessary override_dh_auto_install
    - do not install unneeded C sources.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 17 Dec 2022 14:20:25 +0000

pylibtiff (0.4.4+ds-1) unstable; urgency=medium

  * New upstream release.
  * Update d/watch (new name on PyPI).
  * New debian/source/options file (ignore egg-info).
  * debian/patches:
    - drop patches no longer needed:
      0003-fix-future-division.patch
    - drop patches applied upstreamm: 0002-ctypes.patch,
      0004-Improve-tiff.h-detection.patch and
      0007-Fix-compatibility-with-new-libtiff.patch
    - refresh and renumber remaining patches.
  * debian/rules:
    - disable pybuild clean target.
  * debian/copyright:
    - add a comment to justify the need of Files-Excluded.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 10 Oct 2021 08:23:15 +0000

pylibtiff (0.4.2-8) unstable; urgency=medium

  * debian/patches:
    - new 0007-Fix-compatibility-with-new-libtiff.patch
      (Closes: #989951).
  * debian/control:
    - reformat with cme.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Fri, 01 Oct 2021 06:15:00 +0000

pylibtiff (0.4.2-7) unstable; urgency=medium

  * Drop obsolete get-orig-source script.
  * Set compat to 12.
  * Bump Standards-Version to 4.6.0, no changes.
  * Drop Provides: ${python3:Provides}.
  * Drop Name field from upstream metadata.
  * Set debhelper-compat version in Build-Depends.
  * Explicitly set Rules-Requires-Root: no in debian/control file.
  * Add Forwarded field to patches.
  * Update debiam/watch to advertize re-packaging.
  * debian/patches:
    - set Forwarded tag in all patches.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 26 Sep 2021 09:50:53 +0000

pylibtiff (0.4.2-6) unstable; urgency=medium

  * Team upload.
  * Update gbp.conf to use --source-only-changes by default.
  * Bump Standards-Version to 4.4.0, no changes.
  * Drop Python 2 support.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 21 Jul 2019 21:31:17 +0200

pylibtiff (0.4.2-5) unstable; urgency=medium

  * debian/control:
    - always depend on libtiff-dev

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Wed, 02 Jan 2019 17:58:42 +0000

pylibtiff (0.4.2-4) unstable; urgency=medium

  * debian/control:
    - fix dependency version for libtiff5 (fixes autopkgtest)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 31 Dec 2018 19:08:18 +0000

pylibtiff (0.4.2-3) unstable; urgency=medium

  [ Antonio Valentino ]
  * debian/patches:
    - new 0006-Robust-definition-list-generation.patch.
      The patch avoids permission errors when the definition list is
      generated at runtime. The definition list is no longer written to
      file (tiff_h_X_Y_Z.py) in the system path.

  [ Bas Couwenberg ]
  * Add gbp.conf to use pristine-tar by default.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 31 Dec 2018 09:31:37 +0000

pylibtiff (0.4.2-2) unstable; urgency=medium

  * debian/patches
    - new 0004-Improve-tiff.h-detection.patch: fix detection of the
      tiff.h header on arm* and kfreebsd*
  * Disable bittools extension: it is broken in many platforms.
    Use bitarray instead as backend in the libtiff.lzw module.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 30 Dec 2018 10:35:29 +0000

pylibtiff (0.4.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/rules: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

  [ Antonio Valentino ]
  * New upstream release.
  * Standard version bumped to 4.2.1 (no change).
  * d/control:
    - depend on python-pil instead of python-imaging (Closes: #866462)
    - add build dependency from libtiff-dev
    - add dependency from libtiff5 (Closes: #896388)
    - specify testsuite (autopkgtest-pkg-python)
    - update maintainer and VCS fields (Closes: #705208).
      The package has been adopted by the debian-gis team.
  * Add upstream/metadata file.
  * Switch to pybuild.
  * New package for Python 3.
  * Set compat to 11.
  * d/patches
    - drop 0003-ctypes2.patch and 0004-fix_future.patch: no longer necessary
    - refresh and renumber remaining patches
  * Use pytest for testing.
  * Do not install examples: not provided in the source package from PyPi.
  * Update watch file.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.3.0, no changes.
  * Remove unused examples file.
  * Update rules file, changes:
    - Group and reorder exports
    - Remove disabled export
    - Run dh_numpy* in dh_python* overrides
    - Use $(RM) instead of rm
  * Update watch file to handle common issues.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 23 Dec 2018 20:55:59 +0000

pylibtiff (0.4.1+20160502-1) unstable; urgency=medium

  * QA (group) upload
  * New checkout from Github
    Closes: #838266
  * Rewrote debian/get-orig-source to automatically detect latest change
    date
  * Document exclusion of bitarray code copy
  * Move packaging to Git
  * cme fix dpkg-control
  * Orphan the package
    Closes: #705208
  * hardening=+bindnow

 -- Andreas Tille <tille@debian.org>  Tue, 25 Oct 2016 08:50:11 +0200

pylibtiff (0.4.1~20150805-1) unstable; urgency=medium

  * New VCS checkout
    Closes: #780045
  * Migrated from GoogleCode to Github (rewrote get-orig-source)
  * d/get-orig-source: use xz compression
  * Mathieu declared to leave the team so I add myself as uploader
  * cme fix dpkg-control
  * debhelper 9
  * cme fix dpkg-copyright
  * Injected pull-requests as quilt patches

 -- Andreas Tille <tille@debian.org>  Thu, 24 Dec 2015 08:14:46 +0100

pylibtiff (0.3.0~svn78-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: ndarraytypes.h:681:82: error: operator '<=' has no right
    operand":
    add patch numpy_deprecated_api.patch from upstream svn to handle changes
    in numpy 1.7.
    Thanks to Andrey Rahmatullin for pointing to the upstream commit.
    (Closes: #713588)

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Jul 2013 15:53:16 +0200

pylibtiff (0.3.0~svn78-3) unstable; urgency=low

  * Add support for tiff4. Closes: #663490
  * Bump Std-Vers to 3.9.3, no changes needed
  * Use my @d.o alias, remove DMUA flag for now
  * Add explicit call to dh_numpy to remove an lintian error

 -- Mathieu Malaterre <malat@debian.org>  Fri, 04 May 2012 11:46:51 +0200

pylibtiff (0.3.0~svn78-2) unstable; urgency=low

  * Remove invalid patch. Thanks to Piotr for spotting this. Closes: #649904

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Tue, 29 Nov 2011 12:03:03 +0100

pylibtiff (0.3.0~svn78-1) unstable; urgency=low

  * Initial Debian Upload (Closes: #648769)

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Sun, 20 Nov 2011 11:18:33 +0100
